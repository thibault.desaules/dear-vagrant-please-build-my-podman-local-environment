Vagrant.configure("2") do |config|
  # define our coredns machine
  config.vm.define "coredns", primary: true do |coredns|
    # using a basic ubuntu box 
    coredns.vm.box = "ubuntu/jammy64"
    coredns.vm.box_url = "https://app.vagrantup.com/ubuntu/boxes/jammy64"
    # increase the timeout allowed before failing to have some time to download and configure it
    coredns.vm.boot_timeout = 900
    # define a private network to access the coredns server from our local machine
    coredns.vm.network "private_network", ip: "10.10.0.10"
    # define ansible as provisioner to customize our machine
    coredns.vm.provision :ansible_local do |ansible|
      # configure and define the playbook to use
      ansible.install_mode = "pip"
      ansible.playbook = "ansible/coredns.yml"
    end
    # define some custo on virtualbox
    coredns.vm.provider "virtualbox" do |vbox|
      # like the ressource and hardware
      vbox.memory = 512
      vbox.cpus = 1
      vbox.default_nic_type = "virtio"
      vbox.customize ["modifyvm", :id, "--graphicscontroller", "vmsvga"]
    end
  end
  # define our padman machine
  config.vm.define "podman" do |podman|
    # using the official fedora box to have the podman binary
    podman.vm.box = "fedora/37-cloud-base"
    podman.vm.box_url = "https://app.vagrantup.com/fedora/boxes/37-cloud-base"
    # increase the timeout allowed before failing to have some time to download and configure it
    podman.vm.boot_timeout = 900
    # define a private network to access the podman socket from our local machine
    podman.vm.network "private_network", ip: "10.10.0.20"
    # mount our repositories folders for future use
    podman.vm.synced_folder "C:\\Users\\tdesa\\Repositories", "/repositories"
    # define ansible as provisioner to customize our machine
    podman.vm.provision :ansible_local do |ansible|
      # configure and define the playbook to use
      ansible.install_mode = "pip"
      # special command used to install pip on fedora used to install ansible
      ansible.pip_install_cmd = "sudo dnf install python3-pip"
      ansible.playbook = "ansible/podman.yml"
    end
    # define some custo on virtualbox
    podman.vm.provider "virtualbox" do |vbox|
      # like the ressource and hardware
      vbox.memory = 4096
      vbox.cpus = 4
      vbox.default_nic_type = "virtio"
      vbox.customize ["modifyvm", :id, "--graphicscontroller", "vmsvga"]
    end
  end
end
